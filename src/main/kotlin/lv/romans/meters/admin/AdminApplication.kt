package lv.romans.meters.admin

import lv.romans.meters.commons.Constants
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.ComponentScan
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import java.util.logging.Level
import java.util.logging.Logger
import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.annotation.WebFilter
import javax.servlet.http.HttpServletRequest

@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
@ComponentScan("lv.romans.meters")
class AdminApplication

fun main(args: Array<String>) {
    runApplication<AdminApplication>(*args)
}
