package lv.romans.meters.admin.controllers

import lv.romans.meters.admin.api.PropertyApi
import lv.romans.meters.commons.api.AdminDashboard
import lv.romans.meters.commons.api.AdminProperty
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
class AdminController(val api: PropertyApi) {

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("dashboard")
    fun adminDashboard(): AdminDashboard {
        val properties =
                api.listProperties().properties
                        .map { AdminProperty(it.id, it.address, it.managerEmail) }
        return AdminDashboard(properties)
    }

}