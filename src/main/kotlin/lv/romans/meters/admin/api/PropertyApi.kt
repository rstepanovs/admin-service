package lv.romans.meters.admin.api

import lv.romans.meters.commons.services.PropertyList
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping

@FeignClient("property-service", fallback = HystrixClientFallback::class)
interface PropertyApi {
    @GetMapping("internal/list")
    fun listProperties(): PropertyList
}

internal class HystrixClientFallback : PropertyApi {
    override fun listProperties() = PropertyList(emptyList())
}

